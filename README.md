# README #

### What is this repository for? ###
* Anyline Home assignment
 Single view application Using the GitHub REST API
○ Search bar
○ Show results in a list
○ Scrolling pagination


### MVVM Architecture - Retrofit - Glide - Hilt-Dependency Injection - Paging###

**Application Flow

Search any keyword to Get the data from the RESTAPI using retrofit and display it in the View
Any Item can be clicked and open in the default browser using Intent

**Models  
- UsersResponse -SearchResponse

**ViewModel 
- UsersViewModel

**View 
- MainActivity

**Hilt 
- App is an hilt application with entry point as MainActivity which injects ViewModel

**XMLs 
- ConstrainLayout is used in activity_main because of its better performance and its adoptability for different screen sizes


### Assumptions ###
Integrated Anyline SDK but didn't laid out the surfaceview class because I couldn't aquire the license key

### Tests - Robolectric - Mockk ###*
Few tests written for Adapter class using mockk and robolectric

### Todo's ###
* Respositories //We are not using Presisent Storage so repository is not implemented yet
* Tests
* SurfaceView class for anyline SDK 

### Screenshot ###

![Alt text](/AnylineAssignmentGithub/screenshot.PNG?raw=true "Main Searchable Screen")