package com.one.anylineassignmentgithub.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.recyclerview.widget.RecyclerView
import com.one.anylineassignmentgithub.R
import com.one.anylineassignmentgithub.models.UsersResponse
import com.one.anylineassignmentgithub.paging.SearchPagedDataSourceFactory
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.android.synthetic.main.item_user_profiles.view.*
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.ArgumentMatchers.any

class UsersAdapterTest {

    @MockK lateinit var mockCallback: UsersAdapter.CallbackUserAction
    @MockK lateinit var mockLayoutInflater: LayoutInflater
    @MockK lateinit var mockView: View
    @MockK lateinit var mockPagedList: PagedList<UsersResponse>
    @MockK lateinit var mockUserResponse: UsersResponse
    @MockK lateinit var mockViewHolderUsers: UsersAdapter.ViewHolderUsers


    //Todo
  /*  var searchPagedDataSourceFactory: SearchPagedDataSourceFactory =
        SearchPagedDataSourceFactory("")

    val pagedListConfig = PagedList.Config.Builder()
        .setEnablePlaceholders(false)
        .setInitialLoadSizeHint(15)
        .setPageSize(15)
        .build()

    //var userResponseLiveData: LiveData<PagedList<UsersResponse>> = (LivePagedListBuilder(searchPagedDataSourceFactory, pagedListConfig)).build()
*/
    lateinit var userListAdapter: UsersAdapter

    @Before
    fun setup(){

        MockKAnnotations.init(this, relaxed = true)

        every { mockView.tvScore } returns mockk(relaxed = true)
        every { mockView.tvSiteAdmin } returns mockk(relaxed = true)
        every { mockView.tvLoginName } returns mockk(relaxed = true)
        every { mockView.ivProfileImg } returns mockk(relaxed = true)


        mockkStatic(LayoutInflater::class)
        every { LayoutInflater.from(any()) } returns mockLayoutInflater

        userListAdapter = UsersAdapter(mockCallback)
    }

    @Test
    fun onCreateViewHolder_createsUsersViewHolder_withViewType_2() {
        every { mockLayoutInflater.inflate(R.layout.item_user_profiles, any(), any() ) } returns mockView
        assertTrue(userListAdapter.onCreateViewHolder(mockk(relaxed = true), 2) is UsersAdapter.ViewHolderUsers)
    }

    @Test
    fun onCreateViewHolder_createsLoadingVH_withViewType_0() {
        assertTrue(userListAdapter.onCreateViewHolder(mockk(relaxed = true), 0) is UsersAdapter.LoadingVH)
    }

    //Todo
  /*  @Test
    fun onBindViewHolder_() {
        every { mockPagedList.size } returns 1
        every { mockPagedList[0] } returns mockUserResponse

        userListAdapter.submitList(mockPagedList)

        userListAdapter.onBindViewHolder(mockViewHolderUsers, 0)

        verify { mockViewHolderUsers.itemView }
    }*/
}