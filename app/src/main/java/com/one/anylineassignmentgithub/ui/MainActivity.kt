package com.one.anylineassignmentgithub.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.one.anylineassignmentgithub.R
import com.one.anylineassignmentgithub.adapters.UsersAdapter
import com.one.anylineassignmentgithub.ui.anyline.AnyLineScannerActivity
import com.one.anylineassignmentgithub.utils.Utils
import com.one.anylineassignmentgithub.ui.viewmodels.UsersViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), UsersAdapter.CallbackUserAction {

    private val searchUsersViewModel: UsersViewModel by viewModels()
    private val userListAdapter by lazy { UsersAdapter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //ViewModel to store and manage UI related data in lifecycle conscious way
        searchUsersViewModel.userResponseLiveData.observe(this, {
            it?.let {
                if (it.size > 0) {
                    setUiVisibility(View.GONE)
                }
                userListAdapter.submitList(it)
            }
        })

        //Observer for api error message if any
        searchUsersViewModel.message.observe(this, {
            Toast.makeText(this, "Error in call to Github with message:  ${it}", Toast.LENGTH_SHORT)
                .show()
        })

        //Observer for observing if no item fetched in pagedlist from network call
        searchUsersViewModel.listLimit.observe(this, {
            if (it == 0) {
                setUiVisibility(View.VISIBLE)
                tvInfoSearch.text = getString(R.string.no_result_found)
            } else {
                setUiVisibility(View.GONE)
            }
        })

        rvProfiles.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rvProfiles.itemAnimator = DefaultItemAnimator()
        rvProfiles.adapter = userListAdapter

        etSearchQuery.setOnEditorActionListener { textView, i, keyEvent -> btnSearch.performClick() }

        btnSearch.setOnClickListener {
            if (Utils.isInternetAvailable(this)) {
                if (!etSearchQuery.text.isNullOrEmpty()) {
                    setUiVisibility(View.GONE)
                    searchUsersViewModel.setQuery(etSearchQuery.text.toString().trim())
                    searchUsersViewModel.searchPagedDataSourceFactory.searchPagedDataSource.invalidate()
                    Utils.hideKeyboard(this)

                } else {
                    Toast.makeText(
                        this,
                        getString(R.string.error_please_enter_a_name),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else
                Toast.makeText(
                    this,
                    "Please check your internet connection!",
                    Toast.LENGTH_SHORT
                ).show()
        }

        scan.setOnClickListener{
            openScanner()
        }
    }

    fun setUiVisibility(visibility: Int) {
        rvProfiles.visibility = if (visibility == View.GONE) View.VISIBLE else View.GONE
        tvInfoSearch.visibility = visibility
    }

    override fun onUserClick(position: Int) {
        Toast.makeText(
            this,
            searchUsersViewModel.userResponseLiveData.value!![position]!!.htmlUrl.toString(),
            Toast.LENGTH_SHORT
        ).show()
        Utils.openUrl(
            this,
            searchUsersViewModel.userResponseLiveData.value!![position]!!.htmlUrl.toString()
        )

    }

    fun openScanner(){
            val i = Intent(this, AnyLineScannerActivity::class.java)
            startActivityForResult(i, 1)
        }
}