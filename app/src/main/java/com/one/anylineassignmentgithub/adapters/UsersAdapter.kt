package com.one.anylineassignmentgithub.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.one.anylineassignmentgithub.R
import com.one.anylineassignmentgithub.models.UsersResponse
import kotlinx.android.synthetic.main.item_user_profiles.view.*

class UsersAdapter(var callback: CallbackUserAction) :
    PagedListAdapter<UsersResponse, RecyclerView.ViewHolder>(USERRESPONSE_DIFF_UTIL) {

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE) var networkState = NetworkState.LOADED

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ViewType.VIEW_TYPE_USERS.i) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_user_profiles, parent, false)
            ViewHolderUsers(view, callback)
        } else {
            LoadingVH(
                LayoutInflater.from(parent.context).inflate(R.layout.item_loader, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolderUsers && position != -1) {
            val context = holder.itemView.context
            holder.tvSiteAdmin.text =
                context.getString(
                    R.string.site_admin,
                    if (getAdapterItem(position).siteAdmin!!) "Yes" else "No"
                )
            holder.tvScore.text =
                context.getString(R.string.url, getAdapterItem(position).htmlUrl)
            holder.tvLoginName.text =
                context.getString(R.string.user_name, getAdapterItem(position).login)
            Glide.with(context).load(getAdapterItem(position).avatarUrl)
                .error(R.drawable.ic_placeholder_man)
                .placeholder(R.drawable.progress_anim)
                .into(holder.ivProfileImg)
        }
    }

    private fun isLoading() = networkState == NetworkState.LOADING

    //    maintains and call notify whenever network state changes
    fun setNetworkStatus(state: NetworkState) {
        if (state == networkState) return
        networkState = state
        if (isLoading()) notifyItemInserted(itemCount) else notifyItemRemoved(itemCount)
    }

    private fun getAdapterItem(pos: Int): UsersResponse {
        return if (isLoading() && isLoadingItem(pos)) getItem(pos - 1)!! else getItem(pos)!!
    }

    override fun getItemViewType(position: Int): Int {
        return if (isLoading() && isLoadingItem(position)) ViewType.VIEW_TYPE_LOADER.i else ViewType.VIEW_TYPE_USERS.i
    }

    private fun isLoadingItem(pos: Int) = pos == itemCount - 1

    class ViewHolderUsers(view: View, callback: CallbackUserAction) :
        RecyclerView.ViewHolder(view) {
        var tvScore = view.tvScore
        var tvSiteAdmin = view.tvSiteAdmin
        var tvLoginName = view.tvLoginName
        var ivProfileImg = view.ivProfileImg

        init {
            itemView.setOnClickListener {
                callback.onUserClick(adapterPosition)
            }
        }
    }

    class LoadingVH(view: View) : RecyclerView.ViewHolder(view)

    //callback for getting the itemClick position
    interface CallbackUserAction {
        fun onUserClick(position: Int)
    }

    companion object {
        //Diff util for comparision and pagedlist adapter
        val USERRESPONSE_DIFF_UTIL = object : DiffUtil.ItemCallback<UsersResponse>() {
            override fun areItemsTheSame(oldItem: UsersResponse, newItem: UsersResponse) =
                oldItem === newItem

            override fun areContentsTheSame(oldItem: UsersResponse, newItem: UsersResponse) =
                oldItem == newItem

        }
    }

    enum class NetworkState(val i: Int) {
        LOADING(1),
        LOADED(2)
    }

    enum class ViewType(val i: Int) {
        VIEW_TYPE_LOADER(0),
        VIEW_TYPE_USERS(2)
    }

}